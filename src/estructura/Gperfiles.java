
package estructura;

import Metodos.conexion;
import Metodos.procedimientos_BD_PSICOLOGOS;
import java.awt.HeadlessException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

public final class Gperfiles extends javax.swing.JFrame {
//Para establecer el modelo al JTable
    procedimientos_BD_PSICOLOGOS psicologo = new procedimientos_BD_PSICOLOGOS();
    DefaultTableModel modelo = new DefaultTableModel();
    
    static Connection conn = null;
    //PARA LA CONEXION
    static Statement s = null;
    //PARA ESTABLECER UNA CONSULTA
    static ResultSet rs = null;
    //PARA BRINDAR UN RESULTADO
    
    public Gperfiles() {
        initComponents();
           try {
            //ejecuta la conexion
            conn = conexion.Enlace(conn);
            //ejecuta la consulta
            rs = conexion.PSICOLOGO(rs);
            //volcamos los resultados de rs a rsmetadata
            ResultSetMetaData rsMd = rs.getMetaData();
            //La cantidad de columnas que tiene la TABLA
            int cantidadColumnas = rsMd.getColumnCount();
            //Establecer como cabezeras el nombre de las colunnas
            for (int i = 1; i <= cantidadColumnas; i++) {
                modelo.addColumn(rsMd.getColumnLabel(i));
            }
            rs.close();
            conn.close();
        } catch (SQLException ex) {
        }
        psicologo.mostrar(modelo);
        this.TBADMI.setModel(modelo);
    }

    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnACTADMI = new javax.swing.JButton();
        BTNSALIRADMI = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        TBADMI = new javax.swing.JTable();
        TXTAPELLIDO = new javax.swing.JTextField();
        BTNAGREADMI = new javax.swing.JButton();
        BTNELIMADMI = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();
        jLabel11 = new javax.swing.JLabel();
        TXTNOMBRE = new javax.swing.JTextField();
        TXTALABORABLE = new javax.swing.JTextField();
        TXTDNI = new javax.swing.JTextField();
        TXTGACADEMICO = new javax.swing.JTextField();
        TXTEMAIL = new javax.swing.JTextField();
        TXTTELEFONO = new javax.swing.JTextField();
        TXTPSALARIAL = new javax.swing.JTextField();
        TXTNCOLEGIATURA = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        TXTDISTRITO = new javax.swing.JComboBox();
        TXTSEXO = new javax.swing.JComboBox<>();
        jLabel22 = new javax.swing.JLabel();
        BTNMODIFICAR = new javax.swing.JButton();
        BTNEDITAR = new javax.swing.JButton();
        TXTFECHA = new javax.swing.JTextField();
        TXTEDAD = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        TXTESPECIALIDAD = new javax.swing.JComboBox<>();
        jLabel3 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        btnACTADMI.setText("ACTUALIZAR");
        btnACTADMI.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnACTADMIActionPerformed(evt);
            }
        });
        getContentPane().add(btnACTADMI, new org.netbeans.lib.awtextra.AbsoluteConstraints(650, 370, 120, 40));

        BTNSALIRADMI.setText("ATRAS");
        BTNSALIRADMI.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BTNSALIRADMIActionPerformed(evt);
            }
        });
        getContentPane().add(BTNSALIRADMI, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 630, 108, -1));

        TBADMI.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null}
            },
            new String [] {
                "NOMBRE", "APELLIDOS", "ESPECIALIDAD", "N° DE COLEGIATURA ", "DNI", "DISTRITO", "AÑOS DE EXPERIENCIA ", "FECHA DE NACIMIENTO", "EDAD", "PRESUPUESTO SALARILA", "SEXO", "TELEFONO", "EMAIL", "GRADO ACADEMICO"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, true, false, true, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        TBADMI.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
        TBADMI.setCellSelectionEnabled(true);
        jScrollPane1.setViewportView(TBADMI);
        TBADMI.getColumnModel().getSelectionModel().setSelectionMode(javax.swing.ListSelectionModel.SINGLE_INTERVAL_SELECTION);

        getContentPane().add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 420, 800, 190));

        TXTAPELLIDO.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TXTAPELLIDOActionPerformed(evt);
            }
        });
        getContentPane().add(TXTAPELLIDO, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 170, 156, 27));

        BTNAGREADMI.setText("AGREGAR PERFIL");
        BTNAGREADMI.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BTNAGREADMIActionPerformed(evt);
            }
        });
        getContentPane().add(BTNAGREADMI, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 370, 139, 40));

        BTNELIMADMI.setText("ELIMINAR PERFIL");
        BTNELIMADMI.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BTNELIMADMIActionPerformed(evt);
            }
        });
        getContentPane().add(BTNELIMADMI, new org.netbeans.lib.awtextra.AbsoluteConstraints(340, 360, 140, 40));

        jButton1.setText("CERRAR SESION");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        getContentPane().add(jButton1, new org.netbeans.lib.awtextra.AbsoluteConstraints(670, 630, -1, -1));

        jLabel11.setFont(new java.awt.Font("Tw Cen MT Condensed Extra Bold", 2, 36)); // NOI18N
        jLabel11.setForeground(new java.awt.Color(255, 255, 255));
        jLabel11.setText("REGISTROS DE PSICOLOGOS");
        getContentPane().add(jLabel11, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 10, -1, -1));

        TXTNOMBRE.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TXTNOMBREActionPerformed(evt);
            }
        });
        getContentPane().add(TXTNOMBRE, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 130, 156, 30));

        TXTALABORABLE.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TXTALABORABLEActionPerformed(evt);
            }
        });
        getContentPane().add(TXTALABORABLE, new org.netbeans.lib.awtextra.AbsoluteConstraints(740, 80, 60, 27));

        TXTDNI.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TXTDNIActionPerformed(evt);
            }
        });
        getContentPane().add(TXTDNI, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 90, 100, 27));

        TXTGACADEMICO.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TXTGACADEMICOActionPerformed(evt);
            }
        });
        getContentPane().add(TXTGACADEMICO, new org.netbeans.lib.awtextra.AbsoluteConstraints(680, 197, 156, 30));

        TXTEMAIL.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TXTEMAILActionPerformed(evt);
            }
        });
        getContentPane().add(TXTEMAIL, new org.netbeans.lib.awtextra.AbsoluteConstraints(680, 290, 156, 27));

        TXTTELEFONO.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TXTTELEFONOActionPerformed(evt);
            }
        });
        getContentPane().add(TXTTELEFONO, new org.netbeans.lib.awtextra.AbsoluteConstraints(680, 247, 156, 30));

        TXTPSALARIAL.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TXTPSALARIALActionPerformed(evt);
            }
        });
        getContentPane().add(TXTPSALARIAL, new org.netbeans.lib.awtextra.AbsoluteConstraints(690, 330, 50, 27));

        TXTNCOLEGIATURA.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TXTNCOLEGIATURAActionPerformed(evt);
            }
        });
        getContentPane().add(TXTNCOLEGIATURA, new org.netbeans.lib.awtextra.AbsoluteConstraints(720, 120, 100, 27));

        jLabel2.setFont(new java.awt.Font("Tw Cen MT Condensed Extra Bold", 0, 18)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("Especialidad");
        getContentPane().add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(560, 160, -1, 30));

        jLabel4.setFont(new java.awt.Font("Tw Cen MT Condensed Extra Bold", 0, 18)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(255, 255, 255));
        jLabel4.setText("N° Colegiatura");
        getContentPane().add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(580, 120, -1, -1));

        jLabel5.setFont(new java.awt.Font("Tw Cen MT Condensed Extra Bold", 0, 18)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(255, 255, 255));
        jLabel5.setText("Nombres");
        getContentPane().add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 130, -1, 30));

        jLabel6.setFont(new java.awt.Font("Tw Cen MT Condensed Extra Bold", 0, 18)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(255, 255, 255));
        jLabel6.setText("Apellidos");
        getContentPane().add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 170, -1, -1));

        jLabel7.setFont(new java.awt.Font("Tw Cen MT Condensed Extra Bold", 0, 18)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(255, 255, 255));
        jLabel7.setText("soles/hora");
        getContentPane().add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(750, 330, -1, -1));

        jLabel8.setFont(new java.awt.Font("Tw Cen MT Condensed Extra Bold", 0, 18)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(255, 255, 255));
        jLabel8.setText("Sexo");
        getContentPane().add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 290, -1, -1));

        jLabel9.setFont(new java.awt.Font("Tw Cen MT Condensed Extra Bold", 0, 18)); // NOI18N
        jLabel9.setForeground(new java.awt.Color(255, 255, 255));
        jLabel9.setText("Telefono");
        getContentPane().add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(590, 250, -1, -1));

        jLabel10.setFont(new java.awt.Font("Tw Cen MT Condensed Extra Bold", 0, 18)); // NOI18N
        jLabel10.setForeground(new java.awt.Color(255, 255, 255));
        jLabel10.setText("Email ");
        getContentPane().add(jLabel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(620, 290, -1, -1));

        jLabel13.setFont(new java.awt.Font("Tw Cen MT Condensed Extra Bold", 0, 18)); // NOI18N
        jLabel13.setForeground(new java.awt.Color(255, 255, 255));
        jLabel13.setText("Grado academico");
        getContentPane().add(jLabel13, new org.netbeans.lib.awtextra.AbsoluteConstraints(520, 200, -1, -1));

        jLabel14.setFont(new java.awt.Font("Tw Cen MT Condensed Extra Bold", 0, 18)); // NOI18N
        jLabel14.setForeground(new java.awt.Color(255, 255, 255));
        jLabel14.setText("Fecha de nacimiento");
        getContentPane().add(jLabel14, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 250, -1, -1));

        jLabel15.setFont(new java.awt.Font("Tw Cen MT Condensed Extra Bold", 0, 18)); // NOI18N
        jLabel15.setForeground(new java.awt.Color(255, 255, 255));
        jLabel15.setText("Años laborables ");
        getContentPane().add(jLabel15, new org.netbeans.lib.awtextra.AbsoluteConstraints(570, 80, -1, 30));

        jLabel17.setFont(new java.awt.Font("Tw Cen MT Condensed Extra Bold", 0, 18)); // NOI18N
        jLabel17.setForeground(new java.awt.Color(255, 255, 255));
        jLabel17.setText("Distrito");
        getContentPane().add(jLabel17, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 330, -1, 20));

        jLabel19.setFont(new java.awt.Font("Tw Cen MT Condensed Extra Bold", 0, 18)); // NOI18N
        jLabel19.setForeground(new java.awt.Color(255, 255, 255));
        jLabel19.setText("DNI");
        getContentPane().add(jLabel19, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 90, -1, 30));

        TXTDISTRITO.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Distrito", "Ancon", "Ate", "Barranco", "Breña", "Carabayllo", "Chaclacayo", "Chorrillos", "Cieneguilla", "Comas", "El Agustino", "Independencia", "Jesús María", "La Molina", "La Victoria", "Lima", "Lince", "Los Olivos", "Lurigancho", "Lurín", "Magdalena del Mar", "Miraflores", "Pachacamac", "Pucusana", "Pueblo Libre", "Puente Piedra", "Punta Hermosa", "Punta Negra", "Rimac", "San Bartolo", "San Borja", "San Isidro", "San Juan de Lurigancho", "San Juan de Miraflores", "San Luis", "San Martín de Porres", "San Miguel", "Santa Anita", "Santa María del Mar", "Santa Rosa", "Santiago de Surco", "Surquillo", "Villa El Salvador" }));
        getContentPane().add(TXTDISTRITO, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 330, 170, -1));

        TXTSEXO.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Seleccionar:", "MASCULINO", "FEMENINO" }));
        TXTSEXO.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TXTSEXOActionPerformed(evt);
            }
        });
        getContentPane().add(TXTSEXO, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 290, 170, -1));

        jLabel22.setFont(new java.awt.Font("Tw Cen MT Condensed Extra Bold", 0, 18)); // NOI18N
        jLabel22.setForeground(new java.awt.Color(255, 255, 255));
        jLabel22.setText("Propuesta Salarial");
        getContentPane().add(jLabel22, new org.netbeans.lib.awtextra.AbsoluteConstraints(530, 330, -1, -1));

        BTNMODIFICAR.setText("CONFIRMAR MODIFICACION");
        BTNMODIFICAR.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BTNMODIFICARActionPerformed(evt);
            }
        });
        getContentPane().add(BTNMODIFICAR, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 200, 180, 60));

        BTNEDITAR.setText("EDITAR DATOS");
        BTNEDITAR.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BTNEDITARActionPerformed(evt);
            }
        });
        getContentPane().add(BTNEDITAR, new org.netbeans.lib.awtextra.AbsoluteConstraints(340, 290, 140, 40));
        getContentPane().add(TXTFECHA, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 250, 30, -1));
        getContentPane().add(TXTEDAD, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 220, 40, -1));

        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("EDAD");
        getContentPane().add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 214, 60, 20));

        TXTESPECIALIDAD.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Seleccionar:", "TERAPIA INDIVIDUAL", "TERAPIA DE PAREJAS", "TERAPIA EN ADICCION", "TERAPIA DE FAMILIA" }));
        TXTESPECIALIDAD.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TXTESPECIALIDADActionPerformed(evt);
            }
        });
        getContentPane().add(TXTESPECIALIDAD, new org.netbeans.lib.awtextra.AbsoluteConstraints(680, 160, 150, -1));

        jLabel3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/wallpaper_latino_de_intensamente__by_dwowforce-d8xveoj.jpg"))); // NOI18N
        getContentPane().add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(-290, -50, 1190, 770));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void BTNSALIRADMIActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BTNSALIRADMIActionPerformed
        Eleccion3 m=new Eleccion3();
        m.setLocationRelativeTo(null);
        m.setVisible(true);
        dispose();       
    }//GEN-LAST:event_BTNSALIRADMIActionPerformed

    private void TXTAPELLIDOActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_TXTAPELLIDOActionPerformed
        
    }//GEN-LAST:event_TXTAPELLIDOActionPerformed

    private void BTNELIMADMIActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BTNELIMADMIActionPerformed
        psicologo.eliminar(modelo);
    }//GEN-LAST:event_BTNELIMADMIActionPerformed

    private void BTNAGREADMIActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BTNAGREADMIActionPerformed
    
        int escoger =JOptionPane.showConfirmDialog(this,"Esta seguro que desea registrar","Confirmacion de registar",JOptionPane.YES_NO_OPTION);

                switch(escoger){
                    case JOptionPane.YES_OPTION:
                        psicologo.registrar(modelo);
                        break;
                    case JOptionPane.NO_OPTION:

                }
    }//GEN-LAST:event_BTNAGREADMIActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        Eleccion1 m=new Eleccion1();
        m.setLocationRelativeTo(null);
        m.setVisible(true);
        dispose();         
    }//GEN-LAST:event_jButton1ActionPerformed

    private void TXTNOMBREActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_TXTNOMBREActionPerformed
 
    }//GEN-LAST:event_TXTNOMBREActionPerformed
    private void TXTALABORABLEActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_TXTALABORABLEActionPerformed
    
    }//GEN-LAST:event_TXTALABORABLEActionPerformed
    private void TXTDNIActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_TXTDNIActionPerformed
      
    }//GEN-LAST:event_TXTDNIActionPerformed
    private void TXTGACADEMICOActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_TXTGACADEMICOActionPerformed
     
    }//GEN-LAST:event_TXTGACADEMICOActionPerformed
    private void TXTEMAILActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_TXTEMAILActionPerformed
     
    }//GEN-LAST:event_TXTEMAILActionPerformed
    private void TXTTELEFONOActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_TXTTELEFONOActionPerformed
      
    }//GEN-LAST:event_TXTTELEFONOActionPerformed
    private void TXTPSALARIALActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_TXTPSALARIALActionPerformed
        
    }//GEN-LAST:event_TXTPSALARIALActionPerformed
    private void TXTNCOLEGIATURAActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_TXTNCOLEGIATURAActionPerformed
       
    }//GEN-LAST:event_TXTNCOLEGIATURAActionPerformed
    private void TXTSEXOActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_TXTSEXOActionPerformed
        
    }//GEN-LAST:event_TXTSEXOActionPerformed

    private void BTNMODIFICARActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BTNMODIFICARActionPerformed
        psicologo.editar(modelo);
        //PARA ACTUALIZAR LOS DATOS
        TXTNOMBRE.setText("");
        TXTAPELLIDO.setText("");
        TXTESPECIALIDAD.setSelectedItem("");
        TXTNCOLEGIATURA.setText("");
        TXTDISTRITO.setSelectedItem("");
        TXTALABORABLE.setText("");
        TXTFECHA.setText("");
        TXTEDAD.setText("");
        TXTPSALARIAL.setText("");
        TXTSEXO.setSelectedItem("");
        TXTTELEFONO.setText("");
        TXTEMAIL.setText("");
        TXTGACADEMICO.setText("");
        TXTDNI.setText("");
    }//GEN-LAST:event_BTNMODIFICARActionPerformed

    private void BTNEDITARActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BTNEDITARActionPerformed

        int i = TBADMI.getSelectedRow();

        TXTNOMBRE.setText(modelo.getValueAt(i, 0).toString());
        TXTAPELLIDO.setText(modelo.getValueAt(i,1).toString());
        TXTESPECIALIDAD.setSelectedItem(modelo.getValueAt(i, 2).toString());
        TXTNCOLEGIATURA.setText(modelo.getValueAt(i, 3).toString());
        TXTDNI.setText(modelo.getValueAt(i, 4).toString());
        TXTDISTRITO.setSelectedItem(modelo.getValueAt(i, 5).toString());
        TXTALABORABLE.setText(modelo.getValueAt(i, 6).toString());
        TXTFECHA.setText(modelo.getValueAt(i, 7).toString());
        TXTEDAD.setText(modelo.getValueAt(i, 8).toString());
        TXTPSALARIAL.setText(modelo.getValueAt(i, 9).toString());
        TXTSEXO.setSelectedItem(modelo.getValueAt(i,10).toString());
        TXTTELEFONO.setText(modelo.getValueAt(i,11).toString());
        TXTEMAIL.setText(modelo.getValueAt(i,12).toString());
        TXTGACADEMICO.setText(modelo.getValueAt(i,13).toString());
       
       
    }//GEN-LAST:event_BTNEDITARActionPerformed

    private void btnACTADMIActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnACTADMIActionPerformed
        Gperfiles  Gperfiles=new  Gperfiles();
         Gperfiles.setLocationRelativeTo(null);
         Gperfiles.setVisible(true);
        dispose();
    }//GEN-LAST:event_btnACTADMIActionPerformed

    private void TXTESPECIALIDADActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_TXTESPECIALIDADActionPerformed
        
    }//GEN-LAST:event_TXTESPECIALIDADActionPerformed

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Gperfiles.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Gperfiles.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Gperfiles.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Gperfiles.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new  Gperfiles().setVisible(true);
            }
        });
    }
    
    

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton BTNAGREADMI;
    private javax.swing.JButton BTNEDITAR;
    private javax.swing.JButton BTNELIMADMI;
    private javax.swing.JButton BTNMODIFICAR;
    private javax.swing.JButton BTNSALIRADMI;
    public static javax.swing.JTable TBADMI;
    public static javax.swing.JTextField TXTALABORABLE;
    public static javax.swing.JTextField TXTAPELLIDO;
    public static javax.swing.JComboBox TXTDISTRITO;
    public static javax.swing.JTextField TXTDNI;
    public static javax.swing.JTextField TXTEDAD;
    public static javax.swing.JTextField TXTEMAIL;
    public static javax.swing.JComboBox<String> TXTESPECIALIDAD;
    public static javax.swing.JTextField TXTFECHA;
    public static javax.swing.JTextField TXTGACADEMICO;
    public static javax.swing.JTextField TXTNCOLEGIATURA;
    public static javax.swing.JTextField TXTNOMBRE;
    public static javax.swing.JTextField TXTPSALARIAL;
    public static javax.swing.JComboBox<String> TXTSEXO;
    public static javax.swing.JTextField TXTTELEFONO;
    private javax.swing.JButton btnACTADMI;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JScrollPane jScrollPane1;
    // End of variables declaration//GEN-END:variables
}
