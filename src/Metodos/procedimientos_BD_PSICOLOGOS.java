package Metodos;

import static Metodos.conexion.conn;
import static Metodos.conexion.rs1;
import static estructura.Gperfiles.TBADMI;
import static estructura.Gperfiles.TXTALABORABLE;
import static estructura.Gperfiles.TXTAPELLIDO;
import static estructura.Gperfiles.TXTDISTRITO;
import static estructura.Gperfiles.TXTDNI;
import static estructura.Gperfiles.TXTEDAD;
import static estructura.Gperfiles.TXTEMAIL;
import static estructura.Gperfiles.TXTESPECIALIDAD;
import static estructura.Gperfiles.TXTFECHA;
import static estructura.Gperfiles.TXTGACADEMICO;
import static estructura.Gperfiles.TXTNCOLEGIATURA;
import static estructura.Gperfiles.TXTNOMBRE;
import static estructura.Gperfiles.TXTPSALARIAL;
import static estructura.Gperfiles.TXTSEXO;
import static estructura.Gperfiles.TXTTELEFONO;
import java.awt.HeadlessException;
import java.sql.PreparedStatement;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

public class procedimientos_BD_PSICOLOGOS {
    
    public void mostrar(DefaultTableModel modelo){
        try {
            //ejecuta la conexion
            conn = conexion.Enlace(conn);
            //ejecuta la consulta
            rs1 = conexion.PSICOLOGO(rs1);
            //volcamos los resultados de rs a rsmetadata
            ResultSetMetaData rsMd = rs1.getMetaData();
            //La cantidad de columnas que tiene la TABLA
            int cantidadColumnas = rsMd.getColumnCount();
            
            //Creando las filas para la tabla
            while (rs1.next()) {
                Object[] fila = new Object[cantidadColumnas];
                for (int i = 0; i < cantidadColumnas; i++) {
                    fila[i] = rs1.getObject(i + 1);
          
                }
                modelo.addRow(fila);
            }
            rs1.close();
            conn.close();
        } 
        catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error al mostrar \n" + ex);
        }
    }
    
     public void registrar(DefaultTableModel modelo){
        try {
            conn = conexion.Enlace(conn);
            String sqlinsertar = "insert into PSICOLOGO values (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
            PreparedStatement psta = conn.prepareStatement(sqlinsertar);
            psta.setString(1, TXTNOMBRE.getText());
            psta.setString(2, TXTAPELLIDO.getText());
            psta.setString(3, (String)TXTESPECIALIDAD.getSelectedItem());
            psta.setString(4, TXTNCOLEGIATURA.getText());
            psta.setString(5, TXTDNI.getText());
            psta.setString(6, (String)TXTDISTRITO.getSelectedItem());
            psta.setString(7, TXTALABORABLE.getText());
            psta.setString(8, TXTFECHA.getText());
            psta.setString(9, TXTEDAD.getText());
            psta.setString(10, TXTPSALARIAL.getText());
            psta.setString(11, (String)TXTSEXO.getSelectedItem());
            psta.setString(12, TXTTELEFONO.getText());
            psta.setString(13, TXTEMAIL.getText());
            psta.setString(14, TXTGACADEMICO.getText());
            psta.execute();
            psta.close();
        
            JOptionPane.showMessageDialog(null, "Datos se insertaron en la base de datos");
        } catch (SQLException | HeadlessException ex) {
            System.out.println(ex.getMessage());
        }

        String[] agregar = new String[14];
        agregar[0] = TXTNOMBRE.getText();
        agregar[1] = TXTAPELLIDO.getText();
        agregar[2] = (String)TXTESPECIALIDAD.getSelectedItem();
        agregar[3] = TXTNCOLEGIATURA.getText();
        agregar[4] = TXTDNI.getText();
        agregar[5] = (String)TXTDISTRITO.getSelectedItem();
        agregar[6] = TXTALABORABLE.getText();
        agregar[7] = TXTFECHA.getText();
        agregar[8] = TXTEDAD.getText();
        agregar[9] = TXTPSALARIAL.getText();
        agregar[10] = (String)TXTSEXO.getSelectedItem();
        agregar[11] = TXTTELEFONO.getText();
        agregar[12] = TXTEMAIL.getText();
        agregar[13] = TXTGACADEMICO.getText();
       
            
        modelo.addRow(agregar);
        
        TXTNOMBRE.setText("");
        TXTAPELLIDO.setText("");
        TXTESPECIALIDAD.setSelectedItem("");
        TXTNCOLEGIATURA.setText("");
        TXTDISTRITO.setSelectedItem("");
        TXTALABORABLE.setText("");
        TXTFECHA.setText("");
        TXTEDAD.setText("");
        TXTPSALARIAL.setText("");
        TXTSEXO.setSelectedItem("");
        TXTTELEFONO.setText("");
        TXTEMAIL.setText("");
        TXTGACADEMICO.setText("");
        TXTDNI.setText("");

    }
        
    public void editar(DefaultTableModel modelo){
        try {
            conn = conexion.Enlace(conn);
            String result = "," + "UPDATE PSICOLOGOS SET NOMBRE='" + TXTNOMBRE.getText() + "',"
                    + "APELLIDO='" + TXTAPELLIDO.getText() + "',"
                    + "ESPECIALIDAD='" +(String)TXTESPECIALIDAD.getSelectedItem()+ "',"
                    + "NUM_COLEGIATURA='" + TXTNCOLEGIATURA.getText()+"',"
                    + "DISTRITO='" +((String)TXTDISTRITO.getSelectedItem()) + "',"
                    + "AÑOS_LABORAL=" + TXTALABORABLE.getText() + ","
                    + "FECHA_NAC=" + TXTFECHA.getText()+","
                    + "EDAD=" + TXTEDAD.getText() + ","
                    + "PROPUESTA_SALARIAL=" + TXTPSALARIAL.getText() + ","
                    + "SEXO='" + ((String)TXTSEXO.getSelectedItem()) + "',"
                    + "TELEFONO=" + TXTTELEFONO.getText() + ","
                    + "EMAIL='" + TXTEMAIL.getText() + "',"
                    + "GRADO_ACADEMICO='" + TXTGACADEMICO.getText() + "'"
                    + "WHERE DNI =" +  TXTDNI.getText();
            PreparedStatement psta = conn.prepareStatement(result);

            psta.execute();
            psta.close();

            int elitotal = TBADMI.getRowCount();
            for (int i = elitotal - 1; i >= 0; i--) {
                modelo.removeRow(i);
            }
            mostrar(modelo);

        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Error al actualizar \n" + e);
        }
    }    
    public void eliminar(DefaultTableModel modelo){
      try {
            conn = conexion.Enlace(conn);
            String result = "DELETE FROM PSICOLOGOS WHERE DNI = " +TXTDNI.getText();
            PreparedStatement psta = conn.prepareStatement(result);

            psta.execute();
            psta.close();

             int elitotal=TBADMI.getRowCount();
            for (int i=elitotal-1; i>=0; i--) {
                modelo.removeRow(i);
            }
            mostrar(modelo);

        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Error al actualizar \n" + e);
        }
    }
               
}
